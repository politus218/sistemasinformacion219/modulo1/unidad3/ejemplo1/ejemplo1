﻿-- Ejercicio 1.
-- Realizar un procedimiento almacenado que reciba dos números y te indique el mayor de ellos(realizarle con instrucción if, con consulta de totales y con una función de Mysql).

-- Ejercicio1. Procedimiento con IF.
DELIMITER //
  CREATE OR REPLACE PROCEDURE mayor(n1 int, n2 int)
    BEGIN
      IF n1>n2 THEN
        SELECT n1;
      ELSE
        SELECT n2;
      END IF;
    END //;
DELIMITER ;

CALL mayor (12,23);

-- Ejercicio1. Procedimiento con consulta de totales.

DELIMITER //
  CREATE OR REPLACE PROCEDURE mayorcontotales(a int, b int)
    BEGIN
      CREATE TEMPORARY TABLE IF NOT EXISTS nums(
        num int
                                      );
      INSERT INTO nums (num) VALUES (a), (b);
      SELECT * FROM nums;
      SELECT MAX(num) FROM nums;
    END//
DELIMITER ;

CALL mayorcontotales(12,23);

-- Ejercicio 1. Con una función de Mysql.

DELIMITER //
  CREATE OR REPLACE PROCEDURE mayorconfunc(num1 int, num2 int)
    BEGIN
      SELECT GREATEST(num1,num2);
    END//
DELIMITER;

CALL mayorconfunc(12,23);

-- Ejercicio 2. Realizar un procedimiento almacenado que reciba tres números y dos argumentos de tipo salida donde devuelva el número más grande y el número más pequeño de los tres números pasados.

  DELIMITER //
    CREATE OR REPLACE PROCEDURE mayorymenordetres(n1 int, n2 int, n3 int, OUT mayor int, OUT menor int)
      BEGIN
        SET mayor=GREATEST(n1,n2,n3);
        SET menor=LEAST(n1,n2,n3);
        END //
    DELIMITER;

CALL mayorymenordetres (2,5,7, @mayor, @menor);
SELECT @mayor;
SELECT @menor;

-- Ejercicio 3. Realizar un procedimiento almacenado que reciba dos fechas y te muestre el número de días de diferencia entre las 2 fechas.

  DELIMITER //
    CREATE OR REPLACE PROCEDURE diasdif(fec1 date, fec2 date, OUT resul int)
      BEGIN
      SET resul= datediff(fec1,fec2);
      END //
    DELIMITER ;

CALL diasdif("1977/08/27","1977/05/30", @resul);
SELECT @resul;

-- Ejercicio 4. Realizar un procedimiento almacenado que reciba dos fechas y te muestre
-- el nº de meses de diferencia entre las dos fechas.

DELIMITER //
    CREATE OR REPLACE PROCEDURE mesesdif(fecha1 date, fecha2 date, OUT resul int)
      BEGIN
      SET resul= TIMESTAMPDIFF(month,fecha2,fecha1);
      END //
    DELIMITER ;

CALL mesesdif("1977/09/05","1977/05/30", @resul);
SELECT @resul;

-- Ejercicio 5. Realizar un procedimiento almacenado que reciba dos fechas te devuelva 
-- en tres argumentos de salida los días, meses y años entre las dos fechas.

DELIMITER //
    CREATE OR REPLACE PROCEDURE fechadif(fecha1 date, fecha2 date, OUT diasdif int, OUT mesesdif int, OUT añosdif int)
      BEGIN
      SET diasdif = TIMESTAMPDIFF(DAY,fecha2,fecha1);
      SET mesesdif = TIMESTAMPDIFF(month,fecha2,fecha1);
      SET añosdif = TIMESTAMPDIFF(year,fecha2,fecha1);
      END //
    DELIMITER ;

CALL fechadif("1979/09/05","1977/05/30", @diasdif, @mesesdif, @añosdif);
SELECT @diasdif, @mesesdif, @añosdif;

-- Ejercicio 6. Realizar un procedimiento almacenado que reciba una frase y te muestre
-- el número de caracteres.

DELIMITER //
    CREATE OR REPLACE PROCEDURE cuentaletras(frase varchar(100), OUT resul int)
      BEGIN
      SET resul= LENGTH(frase);
      END //
    DELIMITER ;

CALL cuentaletras("Me llamo tón, Ton-tón", @resul);
SELECT @resul;